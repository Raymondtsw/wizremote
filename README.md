# WiZ Remote #

It's a demonstration for WiZconnected Co., Ltd.

Author
--------
Raymond Tang Shun Wan, Mobile App Developer, loves to develop ***GREAT*** apps.  Raymond started to write his first iOS app in Sep 2009.  Before his engagement to iOS app development, he had over 7 years experiences on IT infrastructure, project management and information security.

Introduction
--------------
Mission statement : Controlling house electronics via iOS mobile app.

Keep everything simple.  I expect the user will only use this electronics control for single-digit second each time.  Just like a TV remote, everything on the same page.  No hierachy, no waiting, no fancy animations.  All instantly showing work or not work.  Therefore, I gave up to use Navigation View.  I just use two container view to display Rooms and Fixtures, and show the weather information on the main page.   Button or switch should be as big as possible.   I use colorful scheme to match with WiZ amazing LED color light bulb.


AC Fixture Problem
-----------------------
To turn on AC when outside temperature is above 25℃, or turn it off when below 25℃.  So I imply if temperature is exactly 25℃, it will do nothing.  (Logic trick ;)

Moreover, the mock API return a mal-formed JSON.  As a quick turnaround, I just use a random number to decide returning back 20℃ or 30℃ locally.

Use Timer inside the app to let weather fetching regularly, i.e. 5 seconds for DEBUG mode and 60 seconds for production mode from real API.


Persistence Requirements
------------------------------
### Persist all rooms list

Use plain file to store the rooms list in JSON data format.  Room list is quite static.  To keep it simple, plain storage is enough as a local cache to load up on screen *before* the data arrives from server API call.  

If it is a more complex model, I will use Core Data or other server synchronized DB, e.g. Realm, Couchbase, AWS AppSync or Google Firebase.

### Persist fixture on off status

Use the user's defaults database to store all switch status.  Combine the ID of room and fixture as a composite unique key. 

UserDefaults is handy and easy to use, compared to Core Data (i.e. no concurrency and bunch of coding).  

Though UserDefaults should not be used for sensitive data because of its security weakness.  In our case, security is not a requirement.


Performance Requirements
--------------------------------
To keep the UI responsive, it's always a good practice to use GCD Async for interface update.  It's quite straight forward.


Architecture Requirements
-------------------------------
Simply iOS MVC   (Model - ViewController - Storyboard).  Use NotificationCenter to notify each other to refresh view and other data events.

### Model ###

Used object-oriented model for room and fixture.    

### ViewController ###

Used UIViewController as main page, with 2 container view of UICollectionView to display list of rooms and fixture.

### Storyboard ###

Used auto-layout for different screen sizing.


Security Issues
----------------
App Transport Security requires all network connection in HTTPS.  Need to upgrade and install SSL  certificate to the API infrastructure to HTTPS for the compliance. 


Known Leftout work
-----------------------
Due to the tight timeline and time availability, this project doesn't include Unit Test Case or UI Test Case files.  In using these unit test technology, we can make buildBot, automated testing and CI with Mac OS Server. 

Graphics and buttons should be customized nice and big.  The trade-off I use is a random selection of nice color dynamically button changes on every app restart.  Match with the LED colorful light bulb which WiZ amazingly built.

A better document can be made on every commit to bitbucket.  Single commit on each functionality. Plus using branch to divide each major changes and merge branch afterwards.


How-To
----------
Download the source from https://bitbucket.org/Raymondtsw/wiz_house/src/master/

For similator testing with Xcode - simply select your device type and press run.

For device testing :

1. Connect your device to Mac with lightning cable.
2. Go to *Project Setting* -> *General*
3. Change your bundle ID, e.g. com.wizconnected.RaymondTangApp
4. Select the correct *Team* under *Signing* section
5. Press *Run*


# Just sharing and wish you enjoy the App ! #

Raymond Tang loves app development and wish to promote native app technology and help corporate to engage in International app market.  

One of my favorite app I made, which featured in Apple Watch launch event in 2015  ( obsoleted since 3 years ago, the server is stopped working after I joined the current job )

https://itunes.apple.com/hk/app/香港交通-vivo/id363743382?mt=8

I can be contacted by raymondtsw (at) gmail.com

Tested and Supported Devices
-----------------------------------
Tailor with iPhone X / SE / 6 / 7 plus, iPad Pro / Air,  both Portrait and Landscape mode  


Remarks:
-----------
WiZ logo source: https://www.businesswire.com/news/home/20180105005863/en/WiZ-Announces-Lifestyle-Smart-Lights-Feature-New

WiZ owns the copyright of the app logo.  The usage of WiZ's logo is solely for demonstration to WiZ only.
