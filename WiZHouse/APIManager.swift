//
//  APIManager.swift
//  WiZHouse
//
//  Created by Raymond Tang on 23/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//

import Foundation
import UIKit

public class APIManager: NSObject
{
    static let shared = APIManager()
    
    // For production : require to use a https connection
    let domain = URL(string: "http://private-1e863-house4591.apiary-mock.com/")
    
    public func getDocumentsDirectory(_ filename:String) -> URL {
        /*
            Public function to return the document directory in app sandbox;
            To persist data inside that will have a backup in User's iCloud
        */
        
        let paths = FileManager.default.urls(for: .documentDirectory, in:.userDomainMask)
        return paths[0].appendingPathComponent(filename, isDirectory: false)
    }

    func getRoomList(completed: @escaping () -> Void) {
        /*
            Retrieve a list of Rooms and Fixtures
            GET http://private-1e863-house4591.apiary-mock.com/rooms
        */
        let request = NSMutableURLRequest(url: URL(string: "rooms", relativeTo: domain)!)
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, res, error) in
            
            if error == nil {
                if let data = data,
                    let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any],
                    let rooms = json!["rooms"] as? [String:Any] {
                    
                    // Save to local as offline cache for later use, e.g. instant load when app starts
                    (rooms as NSDictionary).write(to: self.getDocumentsDirectory("rooms"), atomically: true)
                    
                    completed()
                }
            }
            else {
                print("Error: get rooms list", error!.localizedDescription)
            }
        }).resume()
    }
    
    func jsonToRoomArray(_ jsonArray: [String:Any]) -> [Room] {
        //          In-take json structure:
        //          {
        //            "Bedroom" : {
        //                "fixtures": [
        //                "Light1",
        //                "Light2",
        //                "AC"]
        //            }, ... }
        
        var result = [Room]()
        jsonArray.forEach { (roomName, value) in
            let room = Room()
            room.roomId = roomName.lowercased().replacingOccurrences(of: " ", with: "-")
            room.roomName = roomName
            room.roomColor = .niceColor(START_COLOR_IDX+result.count)
            
            if let value = value as? [String:[String]], let fixtureList = value["fixtures"] {
                var fixtures = [Fixture]()
                
                fixtureList.forEach({ (fixtureName) in
                    let fixture = Fixture()
                    fixture.fixtureId = fixtureName.lowercased()
                    fixture.fixtureName = fixtureName
                    fixture.room = room
                    fixtures.append(fixture)
                })
                
                room.fixtures = fixtures
            }
            result.append(room)
        }
        return result
    }
    
    func switchFixture(_ fixture: Fixture, completed: @escaping (_ success:Bool) -> Void) {
        /*
            Retrieve a list of Rooms and Fixtures
            URI should be in lower case characters and escape space to hypen
            GET http://private-1e863-house4591.apiary-mock.com/:rooms/:fixture/:on
         
            In production, the API should return the reason of returning false, and display to users accordingly.
        */
        
        if let url = URL(string: fixture.room.roomId, relativeTo: domain)?.appendingPathComponent(fixture.fixtureId).appendingPathComponent(fixture.fixtureOnOff ? "on" : "off") {
            
            let request = NSMutableURLRequest(url: url)
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, res, error) in
                
                if error == nil {
                    if let data = data, let result = String(data: data, encoding: .utf8) {
                        completed((result == "true") ? true : false)
                        
                        // In which case API will return false? and how to handle?
                    }
                }
                else {
                    print("Error: switch room fixture on off", error!.localizedDescription)
                    completed(false)
                }
            }).resume()
        }
    }
    
    func loadCurrentWeather() {
        /*
         Retrieve a list of Rooms and Fixtures
         Production GET https://www.metaweather.com/api/location/2165352/
         
         Test GET http://private-1e863-house4591.apiary-mock.com/weather/cold
          or  GET http://private-1e863-house4591.apiary-mock.com/weather/warm
         Return malformed JSON, skip it....
         */
#if DEBUG
        let temp = (arc4random()%2 == 1) ? 20 : 30
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateTemperature"), object: nil, userInfo: ["temp": String(temp)])
        
        if temp > 25 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwitchAC"), object: nil, userInfo: ["status": true])
        }
        else if temp < 25 {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwitchAC"), object: nil, userInfo: ["status": false])
        }
        else {
            // 25℃ do nothing
        }
#else
        let request = NSMutableURLRequest(url: URL(string: "https://www.metaweather.com/api/location/2165352/")!)
        URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, res, error) in
            
            if error == nil {
                if let data = data,
                    let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any],
                    let conso = json!["consolidated_weather"] as? [[String:Any]]
                {
                    if let temp = conso[0]["the_temp"] as? Double {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateTemperature"), object: nil, userInfo: ["temp": String(temp)])
                        
                        // If the temperature is above 25°C, then the AC needs to turn on immediately if it is off.  Vice versa.
                        if temp > 25 {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwitchAC"), object: nil, userInfo: ["status": true])
                        }
                        else if temp < 25 {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SwitchAC"), object: nil, userInfo: ["status": false])
                        }
                        else {
                            // 25℃ do nothing
                        }
                    }
                }
            }
            else {
                print("Error: get rooms list", error!.localizedDescription)
            }
        }).resume()
#endif
    }
}
