//
//  ViewController.swift
//  WiZHouse
//
//  Created by Raymond Tang on 23/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//
//  Display a list of Room, with name show as title

import UIKit

class RoomVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var roomsList: [Room]?
    var currentRoomCell: RoomCell? // Store the selected room cell for UI changes

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadRoomsListFromCache()
        
        // Load the room list from server to see any update
        APIManager.shared.getRoomList  {
            self.loadRoomsListFromCache()
            
            DispatchQueue.main.async {
                // Update UI in main thread
                self.collectionView?.reloadData()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(RoomVC.switchAC(_:)), name: NSNotification.Name(rawValue: "SwitchAC"), object: nil)
    }
        
    func loadRoomsListFromCache() {
        // Load room list from local file cache
        if let rooms = NSDictionary(contentsOf: APIManager.shared.getDocumentsDirectory("rooms")) as? [String:Any] {
            roomsList = APIManager.shared.jsonToRoomArray(rooms)
        }
    }
    
    @objc func switchAC(_ note: Notification) {
        if let onOff = note.userInfo!["status"] as? Bool {
            roomsList?.forEach({ (room) in
                if let fixtures = room.fixtures {
                    fixtures.forEach({ (fixture) in
                        if fixture.fixtureName == "AC" {
                            fixture.fixtureOnOff = onOff
                        }
                    })
                }
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomCell", for: indexPath) as! RoomCell
        if let roomsList = roomsList {
            let room = roomsList[indexPath.row]
            cell.titleLabel.text = room.roomName
            cell.room = room
            
            // Should replace below with nice graphics in production
            cell.thumbLabel.text = String(room.roomName[...room.roomName.startIndex])
            cell.thumbLabel.backgroundColor = room.roomColor
            cell.thumbLabel.layer.cornerRadius = cell.thumbLabel.bounds.width/2.0
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let roomsList = roomsList {
            return roomsList.count
        }
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let newRoomCell = collectionView.cellForItem(at: indexPath) as? RoomCell
        {
            if let currentRoomCell = currentRoomCell {
                currentRoomCell.thumbLabel.layer.borderWidth = 0.0
            }
            
            // Effects to show which room is selected
            newRoomCell.thumbLabel.layer.borderColor = UIColor.black.cgColor
            newRoomCell.thumbLabel.layer.borderWidth = 3.0
            
            currentRoomCell = newRoomCell
            refreshFixtureView(newRoomCell.room)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        // Add inset to the collection view if there are not enough cells to fill the width.
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberOfItems = CGFloat(collectionView.numberOfItems(inSection: section))
        let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
        let padding = (collectionView.frame.width - combinedItemWidth) / 2
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
    
    func refreshFixtureView(_ room: Room) {
        NotificationCenter.default.post(name: Notification.Name("RefreshFixture"), object: room, userInfo: nil)
    }
}

class RoomCell: UICollectionViewCell {
    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var thumbLabel : UILabel! // Should replace thumb text label with nice image in production
    
    var room: Room!
}

let beautifulColors = [[237,2,11],[244,101,40],[255,202,43],[238,47,127],[241,80,140],[255,160,206],[64,120,211],[80,168,227],[173,222,250],[127,176,5],[184,214,4],[204,255,0]]
let START_COLOR_IDX = Int(arc4random()) % beautifulColors.count

extension UIColor {
    // return beautiful color
    static func niceColor(_ index: Int) -> UIColor {
        var idx = index
        if index >= beautifulColors.count {
            idx = index % beautifulColors.count
        }
        
        return UIColor(red: CGFloat(beautifulColors[idx][0])/255.0, green: CGFloat(beautifulColors[idx][1])/255.0, blue: CGFloat(beautifulColors[idx][2])/255.0, alpha: 1.0)
    }
}
