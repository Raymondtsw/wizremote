//
//  FixtureVC.swift
//  WiZHouse
//
//  Created by Raymond Tang on 23/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//
//

import UIKit

class FixtureVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var room: Room?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RefreshFixture"), object: nil, queue: nil) { (notification) in
            if let incomingRoom = notification.object as? Room {
                self.room = incomingRoom
                self.collectionView?.reloadData()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(FixtureVC.switchAC(_:)), name: NSNotification.Name(rawValue: "SwitchAC"), object: nil)
    }
    
    /*
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()        
        // Refresh for rotation
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
 */

    @objc func switchAC(_ note: Notification) {
        if let onOff = note.userInfo!["status"] as? Bool {
            collectionView?.visibleCells.forEach({ (cell) in
                if let fixtureCell = cell as? FixtureCell, fixtureCell.fixture.fixtureName == "AC" {
                    fixtureCell.fixture.fixtureOnOff = onOff
                        
                    DispatchQueue.main.async {
                        fixtureCell.onOffButton.setOn(onOff, animated: true)
                    }
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FixtureCell", for: indexPath) as! FixtureCell
        if let room = room, let fixtureList = room.fixtures {
            cell.fixture = fixtureList[indexPath.row]
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let room = room, let fixtures = room.fixtures {
            return fixtures.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        // Add inset to the collection view if there are not enough cells to fill the width.        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberOfItems = CGFloat(collectionView.numberOfItems(inSection: section))
        let combinedItemWidth = (numberOfItems * flowLayout.itemSize.width) + ((numberOfItems - 1)  * flowLayout.minimumInteritemSpacing)
        let padding = (collectionView.frame.width - combinedItemWidth) / 2
        return UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
    }
}

class FixtureCell: UICollectionViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var onOffButton: UISwitch!
    
    private var _fixture: Fixture!
    var fixture: Fixture! {
        set {
            _fixture = newValue
            titleLabel.text = newValue.fixtureName
            onOffButton.isOn = UserDefaults.standard.bool(forKey: newValue.uniqueId)
            onOffButton.onTintColor = fixture.room.roomColor
        }
        get {
            return _fixture
        }
    }
    
    @IBAction func pressedOnOffSwitch(_ sender: UISwitch) {
        fixture.fixtureOnOff = onOffButton.isOn
        
        APIManager.shared.switchFixture(fixture) { (success) in
            if !success {
                self.fixture.fixtureOnOff = !self.fixture.fixtureOnOff
                
                // Switch back to original state if API returns failed
                DispatchQueue.main.async {
                    // Reset to the previous on off state
                    sender.setOn(self.fixture.fixtureOnOff, animated: true)
                }
            }
        }
    }
}

