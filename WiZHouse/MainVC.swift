//
//  MainVC.swift
//  WiZHouse
//
//  Created by Raymond Tang on 26/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    @IBOutlet var temperatureLabel: UILabel!
    var roomVC: RoomVC!
    var fixtureVC: FixtureVC!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UpdateTemperature"), object: nil, queue: OperationQueue.main) { (note) in
            
            if let temp = note.userInfo!["temp"] as? String {
                self.temperatureLabel.text = temp+"℃"
            }
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        // Refresh for rotation
        roomVC.collectionView?.collectionViewLayout.invalidateLayout()
        fixtureVC.collectionView?.collectionViewLayout.invalidateLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "RoomVC" {
            roomVC = segue.destination as! RoomVC
        }
        else if segue.identifier == "FixtureVC" {
            fixtureVC = segue.destination as! FixtureVC
        }
    }
}
