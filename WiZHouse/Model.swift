//
//  Model.swift
//  WiZHouse
//
//  Created by Raymond Tang on 27/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//

import UIKit

// Object-oriented storing the data

class Fixture: Any {
    var room: Room!
    var fixtureId: String!
    var fixtureName: String!
    var fixtureOnOff: Bool {
        get {
            return UserDefaults.standard.bool(forKey: room.roomId+fixtureId)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: room.roomId+fixtureId)
        }
    }
    var uniqueId: String {
        get {
            return room.roomId+fixtureId
        }
    }
}

class Room: Any {
    var roomId: String!
    var roomColor: UIColor!
    var roomName: String!
    var fixtures: [Fixture]?
}
