//
//  AppDelegate.swift
//  WiZHouse
//
//  Created by Raymond Tang on 23/7/2018.
//  Copyright © 2018 Raymond Tang. All rights reserved.
//

import UIKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var weatherTimer: Timer!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        // Remember to invalide the timer when quiting app
        weatherTimer.invalidate()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Synchronize the default database before the app closed.
        UserDefaults.standard.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

#if DEBUG
        let interval = 5.0
#else
        let interval = 60.0
#endif
        
        // Load the current weather temperature every minute (invoke 60s after)
        weatherTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(AppDelegate.loadCurrentWeather(_:)), userInfo: nil, repeats: true)
        weatherTimer.fire()  // Fire now
    }
    
    @objc func loadCurrentWeather(_ timer: Timer) {
        APIManager.shared.loadCurrentWeather()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        // Synchronize the default database before the app closed.
        UserDefaults.standard.synchronize()
    }


}

